package miage;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Ville {
	@PrimaryKey
	String id;

	@Persistent
	String title;
	@Persistent
	int insee;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getInsee() {
		return insee;
	}
	public void setInsee(int insee) {
		this.insee = insee;
	}
	
	

}
