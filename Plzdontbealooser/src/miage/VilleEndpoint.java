package miage;

import miage.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.datanucleus.query.JDOCursorHelper;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Api(name = "villeendpoint", namespace = @ApiNamespace(ownerDomain = "mycompany.com", ownerName = "mycompany.com", packagePath = "services"))
public class VilleEndpoint {

	/**
	 * This method lists all the entities inserted in datastore.
	 * It uses HTTP GET method and paging support.
	 *
	 * @return A CollectionResponse class containing the list of all entities
	 * persisted and a cursor to the next page.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@ApiMethod(name = "listVille")
	public CollectionResponse<Ville> listVille(
			@Nullable @Named("cursor") String cursorString,
			@Nullable @Named("limit") Integer limit) {

		PersistenceManager mgr = null;
		Cursor cursor = null;
		List<Ville> execute = null;

		try {
			mgr = getPersistenceManager();
			Query query = mgr.newQuery(Ville.class);
			if (cursorString != null && cursorString != "") {
				cursor = Cursor.fromWebSafeString(cursorString);
				HashMap<String, Object> extensionMap = new HashMap<String, Object>();
				extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
				query.setExtensions(extensionMap);
			}

			if (limit != null) {
				query.setRange(0, limit);
			}

			execute = (List<Ville>) query.execute();
			cursor = JDOCursorHelper.getCursor(execute);
			if (cursor != null)
				cursorString = cursor.toWebSafeString();

			// Tight loop for fetching all entities from datastore and accomodate
			// for lazy fetch.
			for (Ville obj : execute)
				;
		} finally {
			mgr.close();
		}

		return CollectionResponse.<Ville> builder().setItems(execute)
				.setNextPageToken(cursorString).build();
	}

	/**
	 * This method gets the entity having primary key id. It uses HTTP GET method.
	 *
	 * @param id the primary key of the java bean.
	 * @return The entity with primary key id.
	 */
	@ApiMethod(name = "getVille")
	public Ville getVille(@Named("id") String id) {
		PersistenceManager mgr = getPersistenceManager();
		Ville ville = null;
		try {
			ville = mgr.getObjectById(Ville.class, id);
		} finally {
			mgr.close();
		}
		return ville;
	}

	/**
	 * This inserts a new entity into App Engine datastore. If the entity already
	 * exists in the datastore, an exception is thrown.
	 * It uses HTTP POST method.
	 *
	 * @param ville the entity to be inserted.
	 * @return The inserted entity.
	 */
	@ApiMethod(name = "insertVille")
	public Ville insertVille(Ville ville) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (containsVille(ville)) {
				throw new EntityExistsException("Object already exists");
			}
			mgr.makePersistent(ville);
		} finally {
			mgr.close();
		}
		return ville;
	}

	/**
	 * This method is used for updating an existing entity. If the entity does not
	 * exist in the datastore, an exception is thrown.
	 * It uses HTTP PUT method.
	 *
	 * @param ville the entity to be updated.
	 * @return The updated entity.
	 */
	@ApiMethod(name = "updateVille")
	public Ville updateVille(Ville ville) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			if (!containsVille(ville)) {
				throw new EntityNotFoundException("Object does not exist");
			}
			mgr.makePersistent(ville);
		} finally {
			mgr.close();
		}
		return ville;
	}

	/**
	 * This method removes the entity with primary key id.
	 * It uses HTTP DELETE method.
	 *
	 * @param id the primary key of the entity to be deleted.
	 */
	@ApiMethod(name = "removeVille")
	public void removeVille(@Named("id") String id) {
		PersistenceManager mgr = getPersistenceManager();
		try {
			Ville ville = mgr.getObjectById(Ville.class, id);
			mgr.deletePersistent(ville);
		} finally {
			mgr.close();
		}
	}

	private boolean containsVille(Ville ville) {
		PersistenceManager mgr = getPersistenceManager();
		boolean contains = true;
		try {
			mgr.getObjectById(Ville.class, ville.getId());
		} catch (javax.jdo.JDOObjectNotFoundException ex) {
			contains = false;
		} finally {
			mgr.close();
		}
		return contains;
	}

	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}

}
