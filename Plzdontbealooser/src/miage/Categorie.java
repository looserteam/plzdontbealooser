package miage;

import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Categorie {
	@PrimaryKey
	String id;

	@Persistent
	String name;
	@Persistent
	String color;
	@Persistent
	String icon;
	@Persistent
	List<String> childrenid;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<String> getChildrenid() {
		return childrenid;
	}

	public void setChildrenid(List<String> childrenid) {
		this.childrenid = childrenid;
	}





}