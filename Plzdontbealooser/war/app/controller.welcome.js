var controller = angular.module('plzdontbealooser-app.controller.welcome', []);

controller.controller('plzdontbealooser-app.controller.welcome', [
		'$scope',
		'GApi',
		'$rootScope',
		'$http',
		function welcomeCtl($scope, GApi, $rootScope, $http) {
			


			$scope.categories = [];
			$scope.categoriesFromGoogle = [];
			$scope.categoriesFiltered = [];

			$scope.villes = [];
			$scope.villesFromGoogle = [];
			$scope.villesFiltered = [];

			$scope.events = [];
			$scope.eventsFromGoogle = [];
			$scope.eventsFiltered = [];

			$scope.data = {};

			$scope.eventsLog = [];

			var log = [];




			
			GApi.execute('eventendpoint', 'listEvent').then(function(resp) {				
				
				$scope.eventsFromGoogle = resp.items;	
				
			}, function() {
				console.log("explosion des quotas google");
			});

			$scope.refreshCategories = function() {

				// agenda mobile
				$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/parameter/AgendaMobile_rootAgendaCatIds').success(function(data, status, headers, config) {

					var listeCategRacine = data[0].value.split(",");

					angular.forEach(listeCategRacine, function(value, key) {

						$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/' + value + '/children?depth=3').success(function(data, status, headers, config) {

							angular.forEach(data, function(value, key) {
								$scope.lectureCategorie(value);
							}, log);

							$scope.resp = data;

						}).error(function(data, status, headers, config) {
							console.log(status);
						});

					}, log);

				}).error(function(data, status, headers, config) {
					console.log(status);
				});

				// agenda loisirs
				$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/parameter/AgendaLoisirs_rootAgendaCatIds').success(function(data, status, headers, config) {

					var listeCategRacine = data[0].value.split(",");

					angular.forEach(listeCategRacine, function(value, key) {

						$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/' + value + '/children?depth=3').success(function(data, status, headers, config) {

							angular.forEach(data, function(value, key) {
								$scope.lectureCategorie(value);
							}, log);

							$scope.resp = data;

						}).error(function(data, status, headers, config) {
							console.log(status);
						});

					}, log);

				}).error(function(data, status, headers, config) {
					console.log(status);
				});

			}
			
			
			
			


			

			
			
			
			

			$scope.refreshVilles = function() {

				$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/city').success(function(data, status, headers, config) {
					angular.forEach(data, function(value, key) {
						$scope.lectureVille(value);
					}, log);

				}).error(function(data, status, headers, config) {
					console.log(status);
				});
			}

			$scope.refreshEvents = function() {

				angular.forEach($scope.categoriesFromGoogle, function(value, key) {

					$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/event/summary?catIds=' + value.id + '&periodOfTime=365&itemsPerPage=5').success(
							function(data, status, headers, config) {

								var currentPage = data.pager.currentPage;
								var nbPages = data.pager.nbPages;

								while (currentPage <= nbPages) {
									$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/event/summary?catIds=' + value.id + '&periodOfTime=365&page=' + currentPage + '&itemsPerPage=5').success(
											function(dataToLoad, status, headers, config) {

												$scope.lectureEvent(dataToLoad);

											})

									currentPage++;
								}

							}).error(function(data, status, headers, config) {
						console.log(status);
					});

				}, log);

			};

			$scope.lectureCategorie = function(categorie) {

				if (typeof categorie.children == 'undefined') {

					$scope.categories.push(categorie);

					GApi.execute('categorieendpoint', 'insertCategorie', categorie).then(function(resp) {
					}, function() {

						console.log("ERROR");
					});

				} else {

					categorie.childrenid = [];

					angular.forEach(categorie.children, function(value, key) {
						categorie.childrenid.push(value.id);
						$scope.lectureCategorie(value);
					}, log);

					$scope.categories.push(categorie);

					GApi.execute('categorieendpoint', 'insertCategorie', categorie).then(function(resp) {
					}, function() {

						console.log("ERROR");
					});

				}

			};

			$scope.lectureVille = function(ville) {

				GApi.execute('villeendpoint', 'insertVille', ville).then(function(resp) {
					$scope.villes.push(ville);
				}, function() {
					console.log("ERROR");
				});

			};

			$scope.lectureEvent = function(eventData) {

				angular.forEach(eventData.data, function(value, key) {

					event = {};

					event.id = value.eventId;
					event.title = value.title;
					event.cityId = value.city.id;
					event.cityName = value.city.title;
					event.categoryId = value.category[0].id;
					event.categoryName = value.category[0].name;
					event.categoryIcon = value.category[0].icon;
					event.startDate = value.startDate;
					event.endDate = value.endDate;
					event.geoLongitude = value.geoLongitude;
					event.geoLatitude = value.geoLatitude;
					event.geoPrecision = value.geoPrecision;

					$scope.events.push(event);

					GApi.execute('eventendpoint', 'insertEvent', event).then(function(resp) {
					}, function() {
						console.log("ERROR");
					});

				}, log);

			};

		}

]);
