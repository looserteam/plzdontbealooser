var router = angular.module('plzdontbealooser-app.router', []);

router.config([ '$urlRouterProvider', function($urlRouterProvider) {

	$urlRouterProvider.otherwise("/login");

} ]);

router.config([ '$stateProvider', function($stateProvider) {

	$stateProvider

	.state('login', {
		url : '/login',
		views : {
			'login' : {
				templateUrl : 'partials/login.html',
				controller : 'plzdontbealooser-app.controller.login',
			},
		},
	})

	.state('home', {
		url : '/',
		views : {
			'navbar' : {
				templateUrl : 'partials/navbar.html',
				controller : 'plzdontbealooser-app.controller.navbar'
			},
			'login' : {
				controller : 'plzdontbealooser-app.controller.home',
				templateUrl : 'partials/home.html'
			},
			'navigation' : {
				templateUrl : 'partials/navigation.html',
				controller : 'plzdontbealooser-app.controller.navigation'
			},
			'welcome' : {
				templateUrl : 'partials/welcome.html',
				controller : 'plzdontbealooser-app.controller.welcome'
			},
			'events' : {
				templateUrl : 'partials/events.html',
				controller : 'plzdontbealooser-app.controller.events'
			},
			'achievements' : {
				templateUrl : 'partials/achievements.html',
				controller : 'plzdontbealooser-app.controller.achievements'
			},
			'ranking' : {
				templateUrl : 'partials/ranking.html',
				controller : 'plzdontbealooser-app.controller.ranking'
			},
			'profil' : {
				templateUrl : 'partials/profil.html',
				controller : 'plzdontbealooser-app.controller.profil'
			},
			'history' : {
				templateUrl : 'partials/history.html',
				controller : 'plzdontbealooser-app.controller.history'
			},
		},
	})

} ]);