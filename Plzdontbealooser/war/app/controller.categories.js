var controller = angular.module('plzdontbealooser-app.controller.categories', []);

controller.controller('plzdontbealooser-app.controller.categories', [ '$scope', 'GApi', '$rootScope', '$http', function categoriesCtl($scope, GApi, $rootScope, $http) {

	
	$scope.categoriesD0 = [];
	$scope.categories = [];
	$scope.categoriesFromGoogle = [];
	$scope.categoriesFiltered = [];

	$scope.data = {};

	$scope.eventsLog = [];

	var log = [];

	// PAGINATION
	// default 10 results per page
	$scope.totalItemsCategories = 0; // changed in api call
	$scope.data.currentPageCategories = 1;

	$scope.setPage = function(pageNo) {
		$scope.data.currentPageCategories = pageNo;
		var begin = (($scope.data.currentPageCategories - 1) * 10)
		var end = begin + 10;
		$scope.categoriesFiltered = $scope.categoriesFromGoogle.slice(begin, end);
	};

	$scope.pageChanged = function() {
		console.log($scope.data.currentPageCategories);
		var begin = (($scope.data.currentPageCategories - 1) * 10)
		var end = begin + 10;
		$scope.categoriesFiltered = $scope.categoriesFromGoogle.slice(begin, end);
	};



	GApi.execute('categorieendpoint', 'listCategorie').then(function(resp) {

		if (typeof resp.items != 'undefined') {
			$scope.categoriesFromGoogle = resp.items;
			$scope.categoriesFromGoogle.sort(function(a, b) {
				return a.name.localeCompare(b.name)
			});
			$scope.categoriesNextPageToken = resp.nextPageToken;

			$scope.totalItemsCategories = $scope.categoriesFromGoogle.length;

			var begin = (($scope.data.currentPageCategories - 1) * 10)
			var end = begin + 10;
			$scope.categoriesFiltered = $scope.categoriesFromGoogle.slice(begin, end);
		}

		
		if (typeof $rootScope.gapi.user.categories != 'undefined') {
			
			$rootScope.categoriesPreferences = [];
			
			angular.forEach($rootScope.gapi.user.categories, function(value, key) {
				
				$rootScope.categoriesPreferences.push($rootScope.getCategorieById(value));

			}, log);

		}

		$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/parameter/AgendaMobile_rootAgendaCatIds').success(function(data, status, headers, config) {

			angular.forEach(data[0].value.split(','), function(value, key) {

				categorie = $rootScope.getCategorieById(value);
				$scope.categoriesD0.push(categorie);

			}, log);

		}).error(function(data, status, headers, config) {
			console.log(status);
		});
		
		$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/parameter/AgendaLoisirs_rootAgendaCatIds').success(function(data, status, headers, config) {

			angular.forEach(data[0].value.split(','), function(value, key) {

				categorie = $rootScope.getCategorieById(value);
				$scope.categoriesD0.push(categorie);

			}, log);

		}).error(function(data, status, headers, config) {
			console.log(status);
		});

	}, function() {
		console.log("explosion des quotas google");
	});

	$scope.showCategoriesD1 = function(id) {

		$scope.categoriesD1 = [];
		$scope.categoriesD2 = [];
		$scope.categoriesD3 = [];
		$scope.categoriesD4 = [];

		categorie = $rootScope.getCategorieById(id);

		angular.forEach(categorie.childrenid, function(value, key) {

			categorieNew = $rootScope.getCategorieById(value);
			$scope.categoriesD1.push(categorieNew);

		}, log);

	}

	$scope.showCategoriesD2 = function(id) {

		$scope.categoriesD2 = [];
		$scope.categoriesD3 = [];
		$scope.categoriesD4 = [];

		categorie = $rootScope.getCategorieById(id);

		angular.forEach(categorie.childrenid, function(value, key) {

			categorieNew = $rootScope.getCategorieById(value);
			$scope.categoriesD2.push(categorieNew);

		}, log);

	}

	$scope.showCategoriesD3 = function(id) {

		$scope.categoriesD3 = [];
		$scope.categoriesD4 = [];

		categorie = $rootScope.getCategorieById(id);

		angular.forEach(categorie.childrenid, function(value, key) {

			categorieNew = $rootScope.getCategorieById(value);
			$scope.categoriesD3.push(categorieNew);

		}, log);

	}

	$scope.showCategoriesD4 = function(id) {

		$scope.categoriesD4 = [];

		categorie = $rootScope.getCategorieById(id);

		angular.forEach(categorie.childrenid, function(value, key) {

			categorieNew = $rootScope.getCategorieById(value);
			$scope.categoriesD4.push(categorieNew);

		}, log);

	}

	$rootScope.getCategorieById = function(id) {

		res = {};
		angular.forEach($scope.categoriesFromGoogle, function(value, key) {
			if (value.id == id) {
				res = value;
			}
		}, log);

		return res;

	}

	$scope.isPreference = function(id) {

		categorie = $rootScope.getCategorieById(id);

		if (typeof $rootScope.gapi.user.categories == 'undefined') {
			$rootScope.gapi.user.categories = [];
		}

		trouve = false;

		angular.forEach($rootScope.gapi.user.categories, function(value, key) {
			if (value == categorie.id) {
				trouve = true;
			}
		}, log);

		return trouve;

	}
	
	


	$scope.addPreference = function(id) {

		$rootScope.gapi.user.categories.push(id);
		
		$rootScope.categoriesPreferences.push($rootScope.getCategorieById(id));
		
		
		$rootScope.updateMyEvents();
		
		

		GApi.execute('userendpoint', 'updateUser', $rootScope.gapi.user).then(function(resp) {

		}, function() {
			console.log("explosion des quotas google");
		});

	}
	
	$scope.removePreference = function(id) {
		
		for(var i = $rootScope.gapi.user.categories.length - 1; i >= 0; i--) {
		    if($rootScope.gapi.user.categories[i] == id) {
		    	$rootScope.gapi.user.categories.splice(i, 1);
		    	$rootScope.categoriesPreferences.splice(i, 1);
		    }
		}	
		
		$rootScope.updateMyEvents();

		GApi.execute('userendpoint', 'updateUser', $rootScope.gapi.user).then(function(resp) {

		}, function() {
			console.log("explosion des quotas google");
		});

	}

	$scope.refreshCategories = function() {

		// agenda mobile
		$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/parameter/AgendaMobile_rootAgendaCatIds').success(function(data, status, headers, config) {

			var listeCategRacine = data[0].value.split(",");

			angular.forEach(listeCategRacine, function(value, key) {

				$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/' + value + '/children?depth=3').success(function(data, status, headers, config) {

					angular.forEach(data, function(value, key) {
						$scope.lectureCategorie(value);
					}, log);

					$scope.resp = data;

				}).error(function(data, status, headers, config) {
					console.log(status);
				});

			}, log);

		}).error(function(data, status, headers, config) {
			console.log(status);
		});

		// agenda loisirs
		$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/parameter/AgendaLoisirs_rootAgendaCatIds').success(function(data, status, headers, config) {

			var listeCategRacine = data[0].value.split(",");

			angular.forEach(listeCategRacine, function(value, key) {

				$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/category/' + value + '/children?depth=3').success(function(data, status, headers, config) {

					angular.forEach(data, function(value, key) {
						$scope.lectureCategorie(value);
					}, log);

					$scope.resp = data;

				}).error(function(data, status, headers, config) {
					console.log(status);
				});

			}, log);

		}).error(function(data, status, headers, config) {
			console.log(status);
		});

	}

	$scope.lectureCategorie = function(categorie) {

		if (typeof categorie.children == 'undefined') {

			$scope.categories.push(categorie);

			GApi.execute('categorieendpoint', 'insertCategorie', categorie).then(function(resp) {
			}, function() {

				console.log("ERROR");
			});

		} else {

			categorie.childrenid = [];

			angular.forEach(categorie.children, function(value, key) {
				categorie.childrenid.push(value.id);
				$scope.lectureCategorie(value);
			}, log);

			$scope.categories.push(categorie);

			GApi.execute('categorieendpoint', 'insertCategorie', categorie).then(function(resp) {
			}, function() {

				console.log("ERROR");
			});

		}

	};

}

]);
