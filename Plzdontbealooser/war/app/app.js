var app = angular.module('plzdontbealooser-app', [

'ui.router', 'angular-google-gapi', 'plzdontbealooser-app.router', 'plzdontbealooser-app.controller', 'ui.bootstrap',

]);

app.run([ 'GAuth', 'GApi', '$state', '$rootScope', '$window', function(GAuth, GApi, $state, $rootScope, $window) {

	var log = [];

	
	
	$rootScope.sendEmail = function(body) {
		

		
		email = "From: Don't Be A looser <dontbealooser@gmail.com>\n" +
				"To: " + $rootScope.gapi.user.name + "<" + $rootScope.gapi.user.email + ">\n" +
				"Subject: Notification Don't Be A Looser \n" +	
				"Message-ID: <dontbealooser@local.machine>\n" +
				"Bonjour, \n \n" + body; 
		
		
		
		
		var base64EncodedEmail = btoa(email);
		base64EncodedEmail.replace(/\+/g, '-').replace(/\//g, '_');
		
		var request = gapi.client.gmail.users.messages.send({
			'userId' : 'me',
			'resource' : {
				'raw' : base64EncodedEmail
			}
		});
		
		request.execute();
		
		$rootScope.addAlert();


	};

	$rootScope.users = [];

	$rootScope.categoriesPreferences = [];

	var CLIENT = '803609262421-cghj492rd1d91ihfeq49dcjkqfu4rdf6.apps.googleusercontent.com';
	var BASE;
	if ($window.location.hostname == 'localhost') {
		BASE = '//localhost:8080/_ah/api';
	} else {
		BASE = 'https://plzdontbealooser.appspot.com/_ah/api';
	}

	GApi.load('userendpoint', 'v1', BASE);
	GApi.load('categorieendpoint', 'v1', BASE);
	GApi.load('villeendpoint', 'v1', BASE);
	GApi.load('eventendpoint', 'v1', BASE);
	GApi.load('calendar', 'v3');
	GApi.load('gmail', 'v1'); 
	GAuth.setClient(CLIENT);
	GAuth.setScope('https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/calendar.readonly https://mail.google.com/gmail.compose https://mail.google.com/gmail.modify');
	GAuth.checkAuth().then(function() {

		GApi.execute('userendpoint', 'getUser', {
			"id" : $rootScope.gapi.user.id
		}).then(function(resp) {
			$rootScope.gapi.user = resp;

			$rootScope.categoriesPreferences = [];

			angular.forEach($rootScope.gapi.user.categories, function(value, key) {

				$rootScope.categoriesPreferences.push($rootScope.getCategorieById(value));

			}, log);

		}, function() {
			console.log("explosion des quotas google");
		});

		if ($state.includes('login')) {
			$state.go('home');
		}
	}, function() {
		$state.go('login');
	});

	$rootScope.logout = function() {
		GAuth.logout().then(function() {
			$state.go('login');
		});
	};

	GApi.execute('userendpoint', 'listUser').then(function(resp) {
		$rootScope.users = resp.items;
	}, function() {
		console.log("explosion des quotas google");
	});
	
	
	
	$rootScope.alerts = [];

	$rootScope.addAlert = function() {
		$rootScope.alerts.push({
			type : 'success',
			msg : 'Notification mail envoyée avec succès !'
		});
	};

	$rootScope.closeAlert = function(index) {
		$rootScope.alerts.splice(index, 1);
	};
	
	

} ]);