var controller = angular.module('plzdontbealooser-app.controller', [

	'plzdontbealooser-app.controller.login',
    'plzdontbealooser-app.controller.home',
    'plzdontbealooser-app.controller.welcome',
    'plzdontbealooser-app.controller.categories',
    'plzdontbealooser-app.controller.villes',
    'plzdontbealooser-app.controller.eventstests',
    'plzdontbealooser-app.controller.navigation',
    'plzdontbealooser-app.controller.navbar',
    'plzdontbealooser-app.controller.events',
    'plzdontbealooser-app.controller.achievements',
    'plzdontbealooser-app.controller.ranking',
    'plzdontbealooser-app.controller.profil',
    'plzdontbealooser-app.controller.history',
    
    ]);

