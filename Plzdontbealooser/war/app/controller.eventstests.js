var controller = angular.module('plzdontbealooser-app.controller.eventstests', []);

controller.controller('plzdontbealooser-app.controller.eventstests', [
		'$scope',
		'GApi',
		'$rootScope',
		'$http',
		function eventsCtl($scope, GApi, $rootScope, $http) {

			$scope.events = [];
			$scope.eventsFromGoogle = [];
			$scope.eventsFiltered = [];

			$scope.data = {};

			$scope.eventsLog = [];

			var log = [];

			// PAGINATION
			// default 5 results per page
			$scope.totalItemsEvents = 0; // changed in api call
			$scope.data.currentPageEvents = 1;

			$scope.setPage = function(pageNo) {
				$scope.data.currentPageEvents = pageNo;
				var begin = (($scope.data.currentPageEvents - 1) * 5)
				var end = begin + 5;
				$scope.eventsFiltered = $scope.eventsFromGoogle.slice(begin, end);
			};

			$scope.pageChanged = function() {
				console.log($scope.data.currentPageEvents);
				var begin = (($scope.data.currentPageEvents - 1) * 5)
				var end = begin + 5;
				$scope.eventsFiltered = $scope.eventsFromGoogle.slice(begin, end);
			};
			
			//participation a un event
			$scope.participer = function(idEvent) {
				if(typeof $rootScope.gapi.user.history == 'undefined'){
					
					$rootScope.gapi.user.history = [];		
					$rootScope.gapi.user.history.push(idEvent);		
				}
				
				else{
					
					$rootScope.gapi.user.history.push(idEvent);
				}
				
				$rootScope.gapi.user.score = $rootScope.gapi.user.score + 10;
				
				$rootScope.sendEmail("Vous avez participé à l'event n° "+ idEvent);
				
				GApi.execute('userendpoint', 'updateUser', $rootScope.gapi.user).then(function(resp) {

					console.log("participation ok");

				}, function() {
					console.log("explosion des quotas google");
				});
				
			};

			GApi.execute('eventendpoint', 'listEvent').then(function(resp) {

				$scope.eventsFromGoogle = resp.items;

				$scope.eventsFromGoogle.sort(function(a, b) {
					return a.title.localeCompare(b.title)
				});
				$scope.totalItemsEvents = $scope.eventsFromGoogle.length;

				var begin = (($scope.data.currentPageEvents - 1) * 5)
				var end = begin + 5;
				$scope.eventsFiltered = $scope.eventsFromGoogle.slice(begin, end);

			}, function() {
				console.log("explosion des quotas google");
			});


			$scope.refreshEvents = function() {

				angular.forEach($scope.categoriesFromGoogle, function(value, key) {

					$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/event/summary?catIds=' + value.id + '&periodOfTime=365&itemsPerPage=5').success(
							function(data, status, headers, config) {

								var currentPageEvents = data.pager.currentPageEvents;
								var nbPages = data.pager.nbPages;

								while (currentPageEvents <= nbPages) {
									$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/event/summary?catIds=' + value.id + '&periodOfTime=365&page=' + currentPageEvents + '&itemsPerPage=5')
											.success(function(dataToLoad, status, headers, config) {

												$scope.lectureEvent(dataToLoad);

											})

									currentPageEvents++;
								}

							}).error(function(data, status, headers, config) {
						console.log(status);
					});

				}, log);

			};

			$scope.lectureEvent = function(eventData) {

				angular.forEach(eventData.data, function(value, key) {

					event = {};

					event.id = value.eventId;
					event.title = value.title;
					event.cityId = value.city.id;
					event.cityName = value.city.title;
					event.categoryId = value.category[0].id;
					event.categoryName = value.category[0].name;
					event.categoryIcon = value.category[0].icon;
					event.startDate = value.startDate;
					event.endDate = value.endDate;
					event.geoLongitude = value.geoLongitude;
					event.geoLatitude = value.geoLatitude;
					event.geoPrecision = value.geoPrecision;

					$scope.events.push(event);

					GApi.execute('eventendpoint', 'insertEvent', event).then(function(resp) {
					}, function() {
						console.log("ERROR");
					});

				}, log);

			};

		}

]);
