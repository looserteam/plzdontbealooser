var controller = angular.module('plzdontbealooser-app.controller.login', []);

controller.controller('plzdontbealooser-app.controller.login', [ '$scope', 'GAuth', 'GData', '$state', function clientList($scope, GAuth, GData, $state) {
	if (GData.isLogin()) {
		$state.go('home');
	}

	$scope.doLogin = function() {
		GAuth.login().then(function() {
			$state.go('home');
		});
	};
} ])