var controller = angular.module('plzdontbealooser-app.controller.villes', []);

controller.controller('plzdontbealooser-app.controller.villes', [ '$scope', 'GApi', '$rootScope', '$http', function welcomeCtl($scope, GApi, $rootScope, $http) {

	$scope.testing="TESTING WORKS";
	$scope.villes = [];
	$scope.villesFromGoogle = [];
	$scope.villesFiltered = [];

	$scope.data = {};

	$scope.eventsLog = [];

	var log = [];

	// PAGINATION
	// default 10 results per page
	$scope.totalItemsVilles = 0; // changed in api call
	$scope.data.currentPageVilles = 1;

	$scope.setPage = function(pageNo) {
		$scope.data.currentPageVilles = pageNo;
		var begin = (($scope.data.currentPageVilles - 1) * 10)
		var end = begin + 10;
		$scope.villesFiltered = $scope.villesFromGoogle.slice(begin, end);
	};

	$scope.pageChanged = function() {
		console.log($scope.data.currentPageVilles);
		var begin = (($scope.data.currentPageVilles - 1) * 10)
		var end = begin + 10;
		$scope.villesFiltered = $scope.villesFromGoogle.slice(begin, end);
	};

	GApi.execute('villeendpoint', 'listVille').then(function(resp) {
		if (typeof resp.items != 'undefined') {
			$scope.villesFromGoogle = resp.items;
			$scope.villesFromGoogle.sort(function(a, b) {
				return a.title.localeCompare(b.title)
			});
			$scope.villesNextPageToken = resp.nextPageToken;

			$scope.totalItemsVilles = $scope.villesFromGoogle.length;

			var begin = (($scope.data.currentPageVilles - 1) * 10)
			var end = begin + 10;
			$scope.villesFiltered = $scope.villesFromGoogle.slice(begin, end);
		}

	}, function() {
		console.log("explosion des quotas google");
	});
	
	
	
	//DEBUT TEMPORAIRE
//	$scope.villesFromGoogle = [
//	  {
//	    "id": "t1_33357",
//	    "title": "Pont-Château",
//	    "insee": 44129
//	  },
//	  {
//	    "id": "t1_10630",
//	    "title": "Châteaubriant",
//	    "insee": 44036
//	  },
//	  {
//	    "id": "t1_10516",
//	    "title": "Haye-Fouassière (La)",
//	    "insee": 44070
//	  },
//	  {
//	    "id": "t1_9995",
//	    "title": "Vue",
//	    "insee": 44220
//	  },
//	  {
//	    "id": "t1_9994",
//	    "title": "Vritz",
//	    "insee": 44219
//	  },
//	  {
//	    "id": "t1_9993",
//	    "title": "Villepot",
//	    "insee": 44218
//	  },
//	  {
//	    "id": "t1_9992",
//	    "title": "Vigneux-de-Bretagne",
//	    "insee": 44217
//	  },
//	  {
//	    "id": "t1_9991",
//	    "title": "Vieillevigne",
//	    "insee": 44216
//	  },
//	  {
//	    "id": "t1_9990",
//	    "title": "Vertou",
//	    "insee": 44215
//	  },
//	  {
//	    "id": "t1_9989",
//	    "title": "Vay",
//	    "insee": 44214
//	  },
//	  {
//	    "id": "t1_9988",
//	    "title": "Varades",
//	    "insee": 44213
//	  },
//	  {
//	    "id": "t1_9987",
//	    "title": "Vallet",
//	    "insee": 44212
//	  },
//	  {
//	    "id": "t1_9986",
//	    "title": "Trignac",
//	    "insee": 44210
//	  },
//	  {
//	    "id": "t1_9985",
//	    "title": "Treillières",
//	    "insee": 44209
//	  }
//	];
//	
//	$scope.villesFromGoogle.sort(function(a, b) {
//		return a.title.localeCompare(b.title)
//	});
//	$scope.totalItemsVilles = $scope.villesFromGoogle.length;
//
//	var begin = (($scope.data.currentPageVilles - 1) * 10)
//	var end = begin + 10;
//	$scope.villesFiltered = $scope.villesFromGoogle.slice(begin, end);
	
	
	//FIN TEMPORAIRE

	$scope.refreshVilles = function() {

		$http.get('http://api.loire-atlantique.fr:80/opendata/1.0/city').success(function(data, status, headers, config) {
			angular.forEach(data, function(value, key) {
				$scope.lectureVille(value);
			}, log);

		}).error(function(data, status, headers, config) {
			console.log(status);
		});
	}

	$scope.lectureVille = function(ville) {

		GApi.execute('villeendpoint', 'insertVille', ville).then(function(resp) {
			$scope.villes.push(ville);
		}, function() {
			console.log("ERROR");
		});

	};

}

]);
